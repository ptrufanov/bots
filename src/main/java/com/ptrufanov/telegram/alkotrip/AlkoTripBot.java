package com.ptrufanov.telegram.alkotrip;

import com.ptrufanov.telegram.alkotrip.model.radar.RadarLocation;
import com.ptrufanov.telegram.alkotrip.service.AdviceService;
import com.ptrufanov.telegram.alkotrip.service.RadarService;
import com.ptrufanov.telegram.alkotrip.util.RadarUtils;
import com.ptrufanov.telegram.common.repository.UpdateRepository;
import com.ptrufanov.telegram.common.service.ErrorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static java.util.Optional.of;

@Component
@RequiredArgsConstructor
@Slf4j
public class AlkoTripBot extends TelegramLongPollingBot {

	private final RadarService radarService;
	private final AdviceService adviceService;

	private final UpdateRepository updateRepository;
	private final ErrorService errorService;

	@Value("${alkotrip.bot.name}")
	private String botName;

	@Value("${alkotrip.bot.api.key}")
	private String apiKey;

	@Override
	public void onUpdateReceived(Update update) {
		of(update)
			.filter(Update::hasMessage)
			.map(Update::getMessage)
			.filter(Message::hasText)
			.map(message -> {
				String text = message.getText();
				try {
					if (text.toLowerCase().contains("погод") || text.toLowerCase().contains("weather")) {
						handleWeatherRequest(update);
					} else if (text.toLowerCase().contains("animation")) {
						handleAnimationRequest(update);
					} else if (text.toLowerCase().contains("совет") || text.toLowerCase().contains("advice")) {
						handleAdviceRequest(update);
					}
				} catch (TelegramApiException e) {
					log.error("An error sending request", e);
					errorService.handleError(update, e);
				}
				return null;
			});
	}

	private void handleWeatherRequest(Update update) throws TelegramApiException {
		try {
			updateRepository.save(update);
			Message message = update.getMessage();
			Chat chat = message.getChat();
			String text = message.getText();
			RadarLocation radarLocation = RadarUtils.getRadarLocationByMessage(text.toLowerCase());
			byte[] imageBytes  = radarService.getRadarImage(radarLocation);
			InputStream image = new ByteArrayInputStream(imageBytes);
			InputFile inputFile = new InputFile(image, "radar");
			SendPhoto sendPhotoRequest = new SendPhoto()
					.setPhoto(inputFile)
					.setChatId(chat.getId());
			execute(sendPhotoRequest);
		} catch (IOException e) {
			log.error("An error reading image bytes", e);
		}
	}

	private void handleAnimationRequest(Update update) throws TelegramApiException {
		updateRepository.save(update);
		Message message = update.getMessage();
		SendAnimation sendAnimationRequest = new SendAnimation()
				.setAnimation("http://wrf.pogoda.by/wrf/images-00/xREFD.gif")
				.setChatId(message.getChat().getId())
				.setHeight(660)
				.setWidth(720);
		execute(sendAnimationRequest);
	}

	private void handleAdviceRequest(Update update) throws TelegramApiException {
		updateRepository.save(update);
		Message message = update.getMessage();
		String advice = adviceService.getRandomAdvice();
		SendMessage sendMessage = new SendMessage(
				message.getChat().getId(),
				message.getFrom().getFirstName() + ", " + advice.toLowerCase()
		);
		execute(sendMessage);
	}

	@Override
	public String getBotUsername() {
		return botName;
	}

	@Override
	public String getBotToken() {
		return apiKey;
	}
}
