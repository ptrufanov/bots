package com.ptrufanov.telegram.alkotrip.util;

import com.ptrufanov.telegram.alkotrip.model.radar.RadarLocation;

import java.util.Arrays;

public class RadarUtils {

	public static RadarLocation getRadarLocationByMessage(String message) {
		return Arrays.stream(RadarLocation.values())
			.filter(radarLocation -> message.contains(radarLocation.getLookupText()))
			.findFirst()
			.orElse(RadarLocation.MINSK);
	}
}
