package com.ptrufanov.telegram.alkotrip.util;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

@Slf4j
public class HttpUtils {

	private static final Integer TIMEOUT = 10 * 1000;

	public static byte[] getByteArrayFromUrl(URL url) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		try {
			byte[] chunk = new byte[4096];
			int bytesRead;
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(TIMEOUT);
			InputStream stream = url.openStream();
			long maxTimeMillis = System.currentTimeMillis() + TIMEOUT;
			while ((System.currentTimeMillis() < maxTimeMillis) && ((bytesRead = stream.read(chunk)) > 0)) {
				outputStream.write(chunk, 0, bytesRead);
			}

		} catch (IOException e) {
			log.error("Error on getting byte array from url " + url.toString());
			return null;
		}

		return outputStream.toByteArray();
	}
}
