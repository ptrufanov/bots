package com.ptrufanov.telegram.alkotrip.model.advice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class AdviceResponse {
    private String status;
    private List<String> errors;
    private List<AdviceData> data;
}
