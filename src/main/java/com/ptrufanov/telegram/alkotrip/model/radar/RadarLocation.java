package com.ptrufanov.telegram.alkotrip.model.radar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public enum RadarLocation {
	MINSK("_minsk", "UMMN"),
	GOMEL("_gomel", "UMGG"),
	BREST("_brest", "UMBB"),
	SMOLENSK("_smolensk", "RUDL");

	private String lookupText;
	private String radarCode;
}
