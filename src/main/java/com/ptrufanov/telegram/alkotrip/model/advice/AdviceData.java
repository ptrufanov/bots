package com.ptrufanov.telegram.alkotrip.model.advice;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AdviceData {
    private Integer id;
    private String text;
    private String html;
    private List<String> tags;
    private List<String> conclusions;
}
