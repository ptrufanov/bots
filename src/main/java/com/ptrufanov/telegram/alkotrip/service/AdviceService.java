package com.ptrufanov.telegram.alkotrip.service;

import com.ptrufanov.telegram.alkotrip.model.advice.AdviceData;
import com.ptrufanov.telegram.alkotrip.model.advice.AdviceResponse;
import com.ptrufanov.telegram.common.service.RestTemplateProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AdviceService {

    private final RestTemplateProvider restTemplateProvider;

    public String getRandomAdvice() {
        RestTemplate restTemplate = restTemplateProvider.restTemplate();
        String result;
        try {
            List<AdviceData> advices = restTemplate.getForObject("http://fucking-great-advice.ru/api/v2/random-advices?limit=1", AdviceResponse.class).getData();
            if (!advices.isEmpty()) {
                AdviceData data = advices.get(0);
                result = data.getText();
            } else {
                result = "Sorry. This service is not available at the moment.";
            }

        } catch (Exception e) {
            result = "Sorry. This service is not available at the moment.";
        }

        return result;
    }
}
