package com.ptrufanov.telegram.alkotrip.service;

import com.ptrufanov.telegram.alkotrip.model.radar.RadarLocation;
import com.ptrufanov.telegram.alkotrip.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Slf4j
@Component
public class RadarService {

    private static final String SERVER_URL = "http://meteoinfo.by/radar/";

    @Value(value="classpath:static/pageTemplateServiceUnavailable.png")
    private Resource defaultImage;

    public byte[]getRadarImage(RadarLocation location) throws IOException {
        String path = SERVER_URL + "?q=" + location.getRadarCode();
        Document doc = Jsoup.connect(path).get();
        Element radar = doc.select("#rdr img").first();
        URL url;
        if (radar != null) {
            String radarUrl = radar.absUrl("src");
            url = new URL(radarUrl);
        } else {
            url = defaultImage.getURL();
        }
        return HttpUtils.getByteArrayFromUrl(url);
    }
}
