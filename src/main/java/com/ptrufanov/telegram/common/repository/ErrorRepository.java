package com.ptrufanov.telegram.common.repository;

import com.ptrufanov.telegram.common.model.Error;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ErrorRepository extends MongoRepository<Error, String> {
}
