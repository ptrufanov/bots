package com.ptrufanov.telegram.common.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface UpdateRepository extends MongoRepository<Update, String> {
}
