package com.ptrufanov.telegram.common.model;

import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.Update;

@Getter
@Setter
public class Error {

    private Update update;
    private String error;

}
