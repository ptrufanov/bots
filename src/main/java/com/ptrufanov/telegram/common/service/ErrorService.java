package com.ptrufanov.telegram.common.service;

import com.ptrufanov.telegram.common.model.Error;
import com.ptrufanov.telegram.common.repository.ErrorRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

@Service
@RequiredArgsConstructor
public class ErrorService {

    private final ErrorRepository errorRepository;

    public void handleError(Update update, Throwable e) {
        Error error = new Error();
        error.setUpdate(update);
        error.setError(ExceptionUtils.getStackTrace(e));
        errorRepository.save(error);
    }
}
