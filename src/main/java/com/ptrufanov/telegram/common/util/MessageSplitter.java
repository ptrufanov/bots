package com.ptrufanov.telegram.common.util;

import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MessageSplitter {

    public static final int MAX_MESSAGE_LENGHT = 4096;

    public static List<String> split(String message) {
        if (!StringUtils.isEmpty(message)) {
            if (message.length() >= MAX_MESSAGE_LENGHT) {
                return Arrays.asList(message.split(String.format("(?<=\\G.{%s})", MAX_MESSAGE_LENGHT)));
            }
        }

        return Collections.singletonList(message);
    }

    public static String truncate(String message) {
        if (!StringUtils.isEmpty(message)) {
            if (message.length() >= MAX_MESSAGE_LENGHT) {
                return message.substring(0, MAX_MESSAGE_LENGHT);
            }
        }
        return message;
    }
}
