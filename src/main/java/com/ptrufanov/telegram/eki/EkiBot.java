package com.ptrufanov.telegram.eki;

import com.ptrufanov.telegram.common.repository.UpdateRepository;
import com.ptrufanov.telegram.common.service.ErrorService;
import com.ptrufanov.telegram.eki.service.EkiTranslateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import static com.ptrufanov.telegram.common.util.MessageSplitter.*;
import static java.util.Optional.of;
import static org.telegram.telegrambots.meta.api.methods.ParseMode.*;

@Component
@Slf4j
@RequiredArgsConstructor
public class EkiBot extends TelegramLongPollingBot {

    private final EkiTranslateService ekiTranslateService;
    private final UpdateRepository updateRepository;
    private final ErrorService errorService;

    @Value("${eki.bot.name}")
    private String botName;

    @Value("${eki.bot.api.key}")
    private String apiKey;

    @Override
    public void onUpdateReceived(Update update) {
        of(update)
            .map(updateRepository::save)
            .filter(Update::hasMessage)
            .map(Update::getMessage)
            .filter(Message::hasText)
            .map(message -> {
                Chat chat = message.getChat();
                String text = message.getText();
                SendMessage messageToSend;
                if ("/start".equals(text)) {
                    messageToSend = new SendMessage(chat.getId(), "Please send a word to translate");
                } else {
                    String translated = truncate(ekiTranslateService.translate(text));
                    if (StringUtils.isEmpty(translated)) {
                        translated = "No translation found";
                    }
                    messageToSend = new SendMessage(chat.getId(), translated).setParseMode(HTML);
                }
                try {
                    execute(messageToSend);
                } catch (Exception e) {
                    log.error("An error on sending translation request", e);
                    errorService.handleError(update, e);
                    try {
                        execute(new SendMessage(chat.getId(), "Sorry, there was an error translating your message"));
                    } catch (TelegramApiException telegramApiException) {
                        log.error("An error on sending translation request", e);
                        errorService.handleError(update, telegramApiException);
                    }
                }
                return null;
            });
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return apiKey;
    }
}
