package com.ptrufanov.telegram.eki.service;

import com.ptrufanov.telegram.eki.model.TranslationResponse;
import com.ptrufanov.telegram.common.service.RestTemplateProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static java.util.Optional.ofNullable;

@Service
@Slf4j
@RequiredArgsConstructor
public class EkiTranslateService {

    private final RestTemplateProvider restTemplateProvider;

    public String translate(String text) {

        RestTemplate restTemplate = restTemplateProvider.restTemplate();
        String query = String.format("https://www.eki.ee/dict/evs/index.cgi?Q=%s&F=M&C06=ru&Z=json", text);
        String result;
        try {
            TranslationResponse response = restTemplate.getForObject(query, TranslationResponse.class);
            result = ofNullable(response)
                    .map(TranslationResponse::getResult)
                    .map(this::cleanupHtml)
                    .orElse("No result");
        } catch (Exception e) {
            log.error("An error has occurred", e);
            result = "Sorry. This service is not available at the moment.";
        }

        return result;
    }

    private String cleanupHtml(String html) {
        Document jsoupDoc = Jsoup.parse(html);
        Document.OutputSettings outputSettings = new Document.OutputSettings();
        outputSettings.prettyPrint(false);
        jsoupDoc.outputSettings(outputSettings);
        jsoupDoc.select("br").before("\\n");
        String str = jsoupDoc.html().replaceAll("\\\\n", "\n");
        return Jsoup.clean(str, "", Whitelist.none(), outputSettings);
    }
}
