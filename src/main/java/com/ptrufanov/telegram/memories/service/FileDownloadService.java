package com.ptrufanov.telegram.memories.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Service
@Slf4j
public class FileDownloadService {

    @Value("${memories.bot.photo.download.path}")
    private String downloadPath;

    public void download(String filePath) {
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
        File file = new File(downloadPath + fileName);
        try {
            log.info("An attempt to download file, " + fileName);
            FileUtils.copyURLToFile(new URL(filePath), file);
            log.info("File downloaded, " + fileName);
        } catch (IOException e) {
            log.error("An error while trying to download file " + fileName, e);
        }
    }
}
