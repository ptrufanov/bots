package com.ptrufanov.telegram.memories;

import com.ptrufanov.telegram.common.repository.UpdateRepository;
import com.ptrufanov.telegram.common.service.ErrorService;
import com.ptrufanov.telegram.memories.service.FileDownloadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.File;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Comparator;

import static java.util.Optional.of;

@Component
@Slf4j
@RequiredArgsConstructor
public class MemoriesBot extends TelegramLongPollingBot {

	private final FileDownloadService fileDownloadService;
	private final UpdateRepository updateRepository;
	private final ErrorService errorService;

	@Value("${memories.bot.name}")
	private String botName;

	@Value("${memories.bot.api.key}")
	private String apiKey;

	@Override
	public void onUpdateReceived(Update update) {
		of(update)
			.map(updateRepository::save)
			.filter(Update::hasMessage)
			.map(Update::getMessage)
			.map(message -> {
				String fileId = "";
				if (message.hasPhoto()) {
					fileId = message
							.getPhoto()
							.stream()
							.max(Comparator.comparing(PhotoSize::getFileSize))
							.map(PhotoSize::getFileId)
							.orElse("");
				} else if (message.hasVideo()) {
					fileId = message
							.getVideo()
							.getFileId();
				}
				GetFile getFile = new GetFile().setFileId(fileId);
				try {
					File file = execute(getFile);
					fileDownloadService.download(file.getFileUrl(getBotToken()));
				} catch (TelegramApiException e) {
					log.error("An exception during downloading the file", e);
					errorService.handleError(update, e);
				}
				return null;
			});
	}

	@Override
	public String getBotUsername() {
		return botName;
	}

	@Override
	public String getBotToken() {
		return apiKey;
	}
}
