. /etc/environment;
cd /opt/telegram/bots;
git pull;
mvn clean package;
cd /opt/docker;
docker-compose stop;
cd /opt/telegram;
rm -rf /opt/docker/*
mv bots/target/bots-2.2.2.RELEASE.jar /opt/docker/bots.jar;
cp bots/docker/* /opt/docker;
cd /opt/docker;
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --build;